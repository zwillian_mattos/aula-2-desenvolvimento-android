package com.willianmattos.android.aula2

import android.os.Bundle
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_basico.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_basico)
        setSupportActionBar(toolbar)

//        textView1.text
//        textView2.text
        button1.setOnClickListener {
            Toast.makeText(this@MainActivity, textView1.text.toString() +  textView2.text.toString(), Toast.LENGTH_LONG).show()
        }
    }

}
                        